<h1>CloudBlast</h1>

## Tentang CloudBlast

CloudBlast merupakan sebuah project berupa website yang saya kerjakan untuk membuat Tugas Akhir semasa kuliah. 
Project ini merupakan sebuah sistem informasi yang digunakan untuk <i>broadcast email</i> menggunakan <i>Framework PHPMailer</i>, dimana setiap orang yang terdaftar dapat menginputkan <i>email</i> dari pelanggan mereka untuk dikirimkan <i>broadcast email</i> berupa promosi atau sebagainya. Project website ini juga sudah mendukung penulisan <i>email</i> berformat seperti halnya penulisa <i>email</i> di Gmail dengan bantuan <i>Framework CKEditor</i>.
