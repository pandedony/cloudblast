<html>
	<header>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Email Blast</title>
		
		
		
		<link rel="stylesheet" type="text/css" href="../style/css/style.css">
		<link rel="stylesheet" type="text/css" href="../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../style/css/attribute.css">
		<link rel="stylesheet" type="text/css" href="../style/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="../style/css/bootstrap.css">
		
		<!-- font -->


	</header>
	<body>
		<!-- navbar -->

		<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:white;">
			<a class="navbar-brand" href="#">
			  <img class="cblogo" src="../assets/img/logo/cb logo.png" alt="CB" style="margin-left:10px;">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			  <ul class="navbar-nav ml-auto">
				<li class="nav-item">
				  <a class="nav-link disabled" href="index.php">BERANDA <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
				  <a class="nav-link disabled" href="tentang.php">TENTANG</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link disabled" href="servis.php">SERVIS</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link disabled" href="kontak.php">KONTAK</a>
				</li>
				<li class="nav-item">
				  <button class="btn btn-outline-success my-2 my-sm-0" type="submit"  onclick="window.location.href='../masuk.php';">MASUK</button>
				</li>
			  </ul>
		   </div>
		</nav>

		<!-- End navbar -->
		


		<main role="main">
		
			
			<div class="section wrapper">
			
				
				
				<div class="section cbLogin">
					<div class="container">
						<div class="row lWrapper rounded-lg mt-4 mb-4">
							<div class="col-lg-5 hResponsive mBannerimg padding_0">
								<img class="mBannerimg rounded-left" src="../assets/img/main/slide05.png">
							</div>
							<div class="col-lg-7 p-5 ">
								<div class="col-lg-12 scbLogin text-center">
									<img class="mx-auto d-block" width="80%" src="../assets/img/logo/CB Logo Text.png">
								</div>
								<div class="col-lg-12 text-center">
									<h4>Reset Password</h4>
								</div>
								<form name="resetpass" id="resetpass" method="post" action="spassword.php">
									<div class="form-group">
										<label for="email" class="sr-only">Email</label>
										<input type="hidden" name="reset" value="<?php echo $alamat=$_GET['kode'];?>">
										<input type="password" name="password" id="Password" class="form-control" placeholder="***********" required>
									</div>
									<div class="form-group mb-4">
										<label for="password" class="sr-only">Password</label>
										<input type="password" name="password2" id="ConfirmPassword" class="form-control" placeholder="***********" required>
									</div>
									<div id="msg"></div>
									<input name="login" id="btnreset" class="btn btn-block btn-outline-success" type="submit" value="Simpan">
								</form>
									
							</div>
						</div>
					</div>
				</div>


				
				<!-- sContact -->
				<div class="section sContact  wFont">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-12 margin-bottom_10">
									KONTAK
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="../assets/img/sContact/location.png">
										</div>
										<div class="full">
											<a href="https://www.google.com/maps/place/8%C2%B040'53.8%22S+115%C2%B010'47.3%22E/@-8.6817107,115.1788095,19z/data=!4m6!3m5!1s0!7e2!8m2!3d-8.6816039!4d115.1798098"  target="_blank">Jalan Tangkuban Perahu no.40
											<br> Denpasar-Bali</a>
											<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="../assets/img/sContact/mail.png">
										</div>
										<div class="full">
										<a href="mailto: pandedonyy@gmail.com">pandedonyy@gmail.com</a>
										<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="../assets/img/sContact/phone.png">
										</div>
										<div class="full">
										<a href="tel: +6287761530973">+62 87 7615 30973</a>
										<p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<form method="pos" id="berlangganan" action="db/simpansubs.php">
									<div class="full margin-bottom_10">
										BERLANGGANAN
									</div>
									<div class="full">
										<input class="form-control mr-sm-0 emailsubs" type="email" name="emailsubs" placeholder="Your Email" aria-label="urmail" required>
									</div>
									<div class="full">
										<input type="submit" class="btn btn-success my-2 my-sm-0" id="btn-sub" >
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- End sContact -->
				
				<!-- Footer -->
				<div class="section footer wFont">
					&copy; 2020 Cloud Blast. All Rights Reserved.
				</div>
				<!-- End Footer -->
				
				
			</div>
			
				
			
		</main>
		
		
	

	<script type="text/javascript" src="../style/js/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../style/js/popper.js"></script>
	<script type="text/javascript" src="../style/js/bootstrap.js"></script>
	<script type="text/javascript" src="../style/js/cbmain.js"></script>
	<script>
		$(document).ready(function(){
        $("#ConfirmPassword").keyup(function(){
             if ($("#Password").val() != $("#ConfirmPassword").val()) {
                 $("#msg").html("Password do not match").css("color","red");
				 $("#btnreset").prop("disabled", true);
             }else{
                 $("#msg").html("Password matched").css("color","green");
				 $("#btnreset").prop("disabled", false);
            }
      });
});
	</script>
	</body>
</html>