<html>
	<header>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Email Blast</title>
		
		
		
		<link rel="stylesheet" type="text/css" href="style/css/style.css">
		<link rel="stylesheet" type="text/css" href="style/css/font.css">
		<link rel="stylesheet" type="text/css" href="style/css/font.css">
		<link rel="stylesheet" type="text/css" href="style/css/attribute.css">
		<link rel="stylesheet" type="text/css" href="style/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="style/css/bootstrap.css">
		
		<!-- font -->


	</header>
	<body>
		<!-- navbar -->

		<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:white;">
			<a class="navbar-brand" href="#">
			  <img class="cblogo" src="assets/img/logo/cb logo.png" alt="CB" style="margin-left:10px;">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			  <ul class="navbar-nav ml-auto">
				<li class="nav-item">
				  <a class="nav-link" href="index.php">BERANDA <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="tentang.php">TENTANG</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="servis.php">SERVIS</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="kontak.php">KONTAK</a>
				</li>
				<li class="nav-item">
				  <button class="btn btn-outline-success my-2 my-sm-0" type="submit"  onclick="window.location.href='masuk.php';">MASUK</button>
				</li>
			  </ul>
		   </div>
		</nav>

		<!-- End navbar -->
		


		<main role="main">
		
			
			<div class="section wrapper">
			
			
				<!-- iBanner -->
				<div class="section iBanner wFont">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 iBannerTxt padding_lay">
								TENTANG
							</div>
						</div>
					</div>
				</div>
				<!-- End iBanner -->
				
				<!-- hText -->
				<div class="section padding_lay">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h2 class="hText">SELAMAT DATANG DI CLOUD BLAST</h2>
							</div>
						</div>
					</div>
				</div>
				<!-- End hText -->
				
				
				<!-- mBanner-->
				<div class="section mBanner">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-6 padding_0 ">
								<img class="mBannerimg" src="assets/img/main/slide04.png">
							</div>
							<div class="col-lg-6 padding_lay  bg-logo wFont">
								<h3 class="sHeading">Visi dan Misi</h3>
								<p>Website ini dibentuk dengan memiliki tujuan untuk membantu pemasaran usaha dibidang produk 
								maupun jasa yang kalian miliki. Dengan Visi "Memberi Manfaat Lebih Dari yang Kalian Bayangkan" kami memiliki 
								kewajiban untuk memberikan layanan dan pelayanan yang terbaik untuk anda. Dengan Visi ini juga kami memiliki
								tujuan untuk memberi manfaat lebih untuk Usaha Anda. Visi ini akan kami wujudkan melalui Misi kami yaitu
								"Memberikan Layanan Pemasaran yang Unggul bagi Pemilik Usaha". Kami akan terus berkembang untuk menjadi yang Terbaik, dan
								melayani anda dengan Baik dan Lebih Baik Lagi.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End mBanner-->
				
				
				<!-- hText -->
				<div class="section padding_lay">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h2 class="hText text-center">MAN BEHIND</h2>
							</div>
						</div>
					</div>
				</div>
				<!-- End hText -->
				
				<!-- Man Behind -->
				<div class="section mBehind">
					<div class="container">
						<div class="row justify-content-md-center">
							<div class="col-lg-4">
								<div class="full">
									<img class="img350px rounded-circle rounded mx-auto d-block" src="assets/img/profile/jadi 1.jpg">
								</div>
								<div class="full text-center">
									<h4> I Made Pande Dony Sudarmadi </h4>
								</div>
								<div class="full text-center">
									Orang Miskin Jadi Sukses ADA! Orang Jelek Jadi Sukses ADA!
									Bahkan Orang Cacat Jadi Sukses Pun ADA! Yang Belum ADA ITU
									ORANG MALAS JADI SUKSES!!!
									<p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Man Behind -->
				
				<!-- sContact -->
				<div class="section sContact  wFont">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-12 margin-bottom_10">
									KONTAK
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/location.png">
										</div>
										<div class="full">
											<a href="https://www.google.com/maps/place/8%C2%B040'53.8%22S+115%C2%B010'47.3%22E/@-8.6817107,115.1788095,19z/data=!4m6!3m5!1s0!7e2!8m2!3d-8.6816039!4d115.1798098"  target="_blank">Jalan Tangkuban Perahu no.40
											<br> Denpasar-Bali</a>
											<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/mail.png">
										</div>
										<div class="full">
										<a href="mailto: pandedonyy@gmail.com">pandedonyy@gmail.com</a>
										<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/phone.png">
										</div>
										<div class="full">
										<a href="tel: +6287761530973">+62 87 7615 30973</a>
										<p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<form method="pos" id="berlangganan" action="db/simpansubs.php">
									<div class="full margin-bottom_10">
										BERLANGGANAN
									</div>
									<div class="full">
										<input class="form-control mr-sm-0 emailsubs" type="email" name="emailsubs" placeholder="Your Email" aria-label="urmail" required>
									</div>
									<div class="full">
										<input type="submit" class="btn btn-success my-2 my-sm-0" id="btn-sub" >
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- End sContact -->
				
				<!-- Footer -->
				<div class="section footer wFont">
					&copy; 2020 Cloud Blast. All Rights Reserved.
				</div>
				<!-- End Footer -->
				
				
			</div>
			
				
			
		</main>
		
		
	

	<script type="text/javascript" src="style/js/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="style/js/popper.js"></script>
	<script type="text/javascript" src="style/js/bootstrap.js"></script>
	<script type="text/javascript" src="style/js/cbmain.js"></script>
	</body>
</html>