<?php
	session_start();
		if(empty($_SESSION['email']&&$_SESSION['level']=='admin'))
		{
			echo "<script> alert('Silahkan Login Terlebih Dahulu!'); location.href='../../masuk.php';</script>";	
		}
		
	include"../../db/koneksi.php";	
	$email=$_SESSION['email'];
	$search = mysqli_query($conn,"select * from user where email='$email'");
	$data = mysqli_fetch_array($search);
	$idtampil=$data['idUser'];
	$idUser= base64_encode($data['idUser']);
?>

<html>
	<header>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Email Blast</title>
		
		
		
		<link rel="stylesheet" type="text/css" href="../../style/css/style.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/styleAU.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/attribute.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/bootstrap.css">
		
		<!-- font -->


	</header>
	<body>
		
		<!-- Navbar Respondsive -->
		<div class="test1he fixed-top">
		<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
		  <!-- Brand -->
		  <a class="navbar-brand" href="#">Navbar</a>

		  <!-- Toggler/collapsibe Button -->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <!-- Navbar links -->
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
			  <li class="nav-item">
				<a class="nav-link" href="editprofil.php">Edit Profil</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="index.php">Format Email</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="kirimemail.php">Kirim Email</a>
			  </li>
			  <li class="nav-item">
					<a class="nav-link" href="member.php">Member</a>
			</li>
				<li class="nav-item">
					<a class="nav-link" href="subcribe.php">Subcriber</a>
				</li>
				<li class="nav-item">
									<a class="nav-link" href="kontakmasuk.php">Kontak Masuk</a>
								</li>
			  <li class="nav-item">
				<a class="nav-link" href="../../db/logout.php">Logout</a>
			  </li>
			</ul>
			
		  </div>
		 </nav>
		</div>
		<!-- End Navbar Sepondsive -->
		
		
		<!-- navbartop -->
		<div class="section navbartop fixed-top">
				<div class="nLeft bg-dark">
					<img width="150px;" class="navbartopimg" src="../../assets/img/logo/CB Logo Text Light.png">
				</div>
				<div class="nRight bg-dark text-right">
				<div class="dropdown">
				  <button type="button" class="btn btn-light dropdown-toggle wFont" data-toggle="dropdown">
					<?php echo $data['namadpn']; ?>
				  </button>
				  <div class="dropdown-menu">
					<a class="dropdown-item" href="EditProfil.php">Edit Profile</a>
					<a class="dropdown-item" href="../../db/logout.php">Logout</a>
				  </div>
				</div>					
				</div>
		
		</div>
		<!-- End navbartop -->
		
		
		
		<main role="main">
			<div class="section wrapper">
				
				
				<!-- Navbar Left -->
				<div class="section navbarleft fixed-top bgcwhite">
					<div class="container">
						<div class="row">
							<ul class="navbar-nav col-lg-12 p-2">
								<li class="nav-item mt-2">
									<a href="index.php" class="nav-link" href="#">Format Email</a>
								</li>
								<li class="nav-item mt-2">
									<a class="nav-link" href="kirimemail.php">Kirim Email</a>
								</li>
								<li class="nav-item mt-2">
									<a class="nav-link">Anggota</a>
									<ul class="navbar-nav col-lg-12 p-2">
										<li class="nav-item mt-2">
											<a class="nav-link" href="member.php">Member</a>
										</li>
										<li class="nav-item mt-2">
											<a class="nav-link" href="subcribe.php">Subcriber</a>
										</li>
									</ul>
								</li>
								<li class="nav-item mt-2">
									<a class="nav-link" href="kontakmasuk.php">Kontak Masuk</a>
								</li>
								<li class="nav-item mt-2">
									<a href="../../db/logout.php" class="nav-link" href="#">Keluar</a>
									
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END Navbar Left -->
				
				<div class="section mainright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 mt-3"> 
								<form id="kirimeamil" name="kirimeamil" method="post" class="kirimeamil" id="kirimeamil" action="simpankirim.php">
									<div class="row mt-2 p-2">
										<div class="col-lg-2  p-1 text-center">
										Judul Format Email :
										</div>
										<div class=" col-lg-2">
											<select class="form-control" name="option1" id="exampleFormControlSelect1" required>
											 <?php 
											  $datajudul = mysqli_query($conn, "select * from emailformat where idUser=$idtampil");
											while($d=mysqli_fetch_array($datajudul)){
											 ?>
											   <option name="<?php echo $d['idFormat']?>" value="<?php echo $d['idFormat']?>" required><?php echo $d['title']?></option> 
											 <?php
											  }
											 ?>
											</select>

										</div>
										<div class=" col-lg-2">
											<div class="form-check">
											  <input class="form-check-input" type="radio" name="scheduling" id="exampleRadios1" value="sekarang" onclick="disableks()" checked>
											  <label class="form-check-label" for="exampleRadios1">
												Kirim Sekarang
											  </label>
											</div>
											<div class="form-check">
											  <input class="form-check-input" type="radio" name="scheduling" id="exampleRadios1" value="nanti" onclick="enebleks()" checked>
											  <label class="form-check-label" for="exampleRadios1">
												Kirim Nanti
											  </label>
											</div>
										</div>
										<div class=" col-lg-3">
											<input type="datetime-local" name="tglter" id="myCheck" value="tangga" required>
										</div>
										<div class="col-lg-12 text-center pt-4">
											<h3>Kirim Ke?</h3>
										</div>
										<div class="col-lg-12 pt-2">

											<input type="checkbox" class="mt-2 mb-2"  onchange="checkAll(this)" name="chk[]"><b> Kirim  ke Semua</b>
											<br><b>Member</b>
											<?php
											  $kolom = 3;    
											  $i=1;       
											  $data2 = mysqli_query($conn, "select * from user where level='user'");
											  
											  while($d=mysqli_fetch_array($data2)){
												if(($i) % $kolom== 1) {    
												echo'<div class="row bgcwhite">';     
												}  
												echo '<div class="col-sm-4">
												<input type="hidden" name="idtampil" value="'.$idtampil.'">
												<input type="checkbox" name="idemailp[]" value="'.$d['idUser'].'">&nbsp;'.$d['email'].'</div>';
												if(($i) % $kolom== 0) {    
												echo'</div>';        
												}
											  $i++;
											  }
											  ?>
											  
											  
										</div>
										</div>
										<div class="col-lg-12 pt-2">
											
											<b>Subscriber</b>
											<?php
											  $kolom = 3;    
											  $i=1;       
											  $data21 = mysqli_query($conn, "select * from subcribe");
											  
											  while($d1=mysqli_fetch_array($data21)){
												if(($i) % $kolom== 1) {    
												echo'<div class="row bgcwhite">';     
												}  
												echo '<div class="col-sm-4">
												<input type="hidden" name="idtampil" value="'.$idtampil.'">
												<input type="checkbox" name="idsub[]" value="'.$d1['idsubcribe'].'">&nbsp;'.$d1['emailsubcribe'].'</div>';
												if(($i) % $kolom== 0) {    
												echo'</div>';        
												}
											  $i++;
											  }
											  ?>
											  
											  
										</div>
										</div>
										<div class="form-group col-lg-12 mt-4 text-center">
											<input type="submit" id="btn-simpan" name="submit" Value="Kirim" class="btn btn-block btn-outline-success btn-simpan">
										</div>
										
									</div>
								</form>
									<div class="col-lg-12 tampilantrian">
										
									</div>
									<div class="col-lg-12 tampilhistory">
										
									</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</main>
	
	
	

	<script type="text/javascript" src="../../style/js/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../style/js/popper.js"></script>
	<script type="text/javascript" src="../../style/js/bootstrap.js"></script>
	<script type="text/javascript" src="../../style/js/cbmain.js"></script>
	<script>
	$(document).ready(function(){
		$('.tampilhistory').load('tampilhistory.php');	
		$('.tampilantrian').load('tampilantrian.php');	
	});		
		function disableks() {
  document.getElementById("myCheck").disabled = true;

}

function enebleks() {
  document.getElementById("myCheck").disabled = false;

}


function checkAll(ele) {
      var checkboxes = document.getElementsByTagName('input');
      if (ele.checked) {
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i].type == 'checkbox' ) {
                  checkboxes[i].checked = true;
              }
          }
      } else {
          for (var i = 0; i < checkboxes.length; i++) {
              if (checkboxes[i].type == 'checkbox') {
                  checkboxes[i].checked = false;
              }
          }
      }
  }
	</script>
	</body>
</html>