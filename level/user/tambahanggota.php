<?php
	session_start();
		if(empty($_SESSION['email']&&$_SESSION['level']=='user'))
		{
			echo "<script> alert('Silahkan Login Terlebih Dahulu!'); location.href='../../masuk.php';</script>";	
		}
		
	include"../../db/koneksi.php";	
	$email=$_SESSION['email'];
	$search = mysqli_query($conn,"select * from user where email='$email'");
	$data = mysqli_fetch_array($search);
	$idtampil=$data['idUser'];
	$idUser= base64_encode($data['idUser']);
?>

<html>
	<header>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Email Blast</title>
		
		
		
		<link rel="stylesheet" type="text/css" href="../../style/css/style.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/styleAU.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/font.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/attribute.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="../../style/css/bootstrap.css">
		
		<!-- font -->


	</header>
	<body>
		
		<!-- Navbar Respondsive -->
		<div class="test1he fixed-top">
		<nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
		  <!-- Brand -->
		  <a class="navbar-brand" href="#">Navbar</a>

		  <!-- Toggler/collapsibe Button -->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <!-- Navbar links -->
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			<ul class="navbar-nav">
			  <li class="nav-item">
				<a class="nav-link" href="editprofil.php">Edit Profil</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="index.php">Format Email</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="kirimemail.php">Kirim Email</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="tambahanggota.php">Tambah Anggota</a>
				
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="../../db/logout.php">Logout</a>
			  </li>
			</ul>
			
		  </div>
		 </nav>
		</div>
		<!-- End Navbar Sepondsive -->
		
		
		<!-- navbartop -->
		<div class="section navbartop fixed-top">
				<div class="nLeft bg-dark">
					<img width="150px;" class="navbartopimg" src="../../assets/img/logo/CB Logo Text Light.png">
				</div>
				<div class="nRight bg-dark text-right">
				<div class="dropdown">
				  <button type="button" class="btn btn-light dropdown-toggle wFont" data-toggle="dropdown">
					<?php echo $data['namadpn']; ?>
				  </button>
				  <div class="dropdown-menu">
					<a class="dropdown-item" href="EditProfil.php">Edit Profile</a>
					<a class="dropdown-item" href="../../db/logout.php">Logout</a>
				  </div>
				</div>					
				</div>
		
		</div>
		<!-- End navbartop -->
		
		
		
		<main role="main">
			<div class="section wrapper">
				
				
				<!-- Navbar Left -->
				<div class="section navbarleft fixed-top bgcwhite">
					<div class="container">
						<div class="row">
							<ul class="navbar-nav col-lg-12 p-2">
								<li class="nav-item mt-2">
									<a href="index.php" class="nav-link" href="#">Format Email</a>
								</li>
								<li class="nav-item mt-2">
									<a class="nav-link" href="kirimemail.php">Kirim Email</a>
								</li>
								<li class="nav-item mt-2">
									<a class="nav-link" href="tambahanggota.php">Tambah Pelanggan</a>
								</li>
								<li class="nav-item mt-2">
									<a href="../../db/logout.php" class="nav-link" href="#">Keluar</a>
									
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END Navbar Left -->
				
				<div class="section mainright">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 mt-3"> 
								<form id="formtemail" name="formtemail" method="post" class="formtemail" id="formtemail" action="simpanpelanggan.php">
									<div class="row mt-2 p-2">
										
										<div class="col-lg-1  p-1 text-center">
										Nama :
										</div>
										<div class=" col-lg-3">
											<input type="text" name="namap" class="form-control" required placeholder="Tambah Nama Disini...">
										</div>
										<div class="col-lg-1  p-1 text-center">
										Email :
										</div>
										<div class=" col-lg-3">
											<input type="hidden" name="idUser" class="form-control" value="<?php echo $idUser; ?>">
											<input type="email" name="pelanggan" class="form-control" required placeholder="Tambah Email Disini...">
										</div>
										<div class="form-group col-lg-3 mt-lg-0 mt-md-1 text-center">
											<input type="submit" id="btn-simpan" name="submit" Value="Tambah" class="btn btn-block btn-outline-success btn-simpan">
										</div>
									</div>
								</form>
							</div>
							<div class="col-lg-12 tampilanggota">

							</div>
						</div>
					</div>
				</div>
					
				
			</div>
		</main>
	
	
	

	<script type="text/javascript" src="../../style/js/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="../../style/js/popper.js"></script>
	<script type="text/javascript" src="../../style/js/bootstrap.js"></script>
	<script type="text/javascript" src="../../style/js/cbmain.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.tampilanggota').load('tampilanggota.php');			
	});		

	</script>
	</body>
</html>