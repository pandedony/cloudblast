<html>
	<header>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Email Blast</title>
		
		
		
		<link rel="stylesheet" type="text/css" href="style/css/style.css">
		<link rel="stylesheet" type="text/css" href="style/css/font.css">
		<link rel="stylesheet" type="text/css" href="style/css/font.css">
		<link rel="stylesheet" type="text/css" href="style/css/attribute.css">
		<link rel="stylesheet" type="text/css" href="style/css/responsive.css">
		<link rel="stylesheet" type="text/css" href="style/css/bootstrap.css">
		
		<!-- font -->


	</header>
	<body>
		<!-- navbar -->

		<nav class="navbar navbar-expand-lg navbar-light fixed-top" style="background-color:white;">
			<a class="navbar-brand" href="#">
			  <img class="cblogo" src="assets/img/logo/cb logo.png" alt="CB" style="margin-left:10px;">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			  <ul class="navbar-nav ml-auto">
				<li class="nav-item">
				  <a class="nav-link" href="index.php">BERANDA <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="tentang.php">TENTANG</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="servis.php">SERVIS</a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="kontak.php">KONTAK</a>
				</li>
				<li class="nav-item">
				  <button class="btn btn-outline-success my-2 my-sm-0" type="submit"  onclick="window.location.href='masuk.php';">MASUK</button>
				</li>
			  </ul>
		   </div>
		</nav>

		<!-- End navbar -->
		


		<main role="main">
		
			
			<div class="section wrapper">
			
			
				<!-- iBanner -->
				<div class="section iBanner wFont">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 iBannerTxt padding_lay">
								SERVIS
							</div>
						</div>
					</div>
				</div>
				<!-- End iBanner -->
				
				<!-- hText -->
				<div class="section padding_lay">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h2 class="hText">KAMI BISA MEMBANTU PERTUMBUHAN BISNIS</h2>
							</div>
						</div>
					</div>
				</div>
				<!-- End hText -->
				
				
				<!-- mBanner-->
				<div class="section mBanner">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-6 padding_0 ">
								<img class="mBannerimg" src="assets/img/main/slide03.jpg">
							</div>
							<div class="col-lg-6 padding_lay  bg-logo wFont">
								<h3 class="sHeading">Manfaat Email Marketing 
								<br>di dalam Bisnis</h3>
								<p>Dalam pemasaran bisnis di era digital, 
								memaksimalkan semua kanal pemasaran dipandang perlu untuk menaikkan angka penjualan. 
								Salah satunya dengan memanfaatkan email marketing. Para pemilik bisnis online sudah lama mengenal alternatif pemasaran ini, 
								yaitu dengan memakai media email atau surat elektronik kepada calon pelanggan dan pelanggan yang sudah pernah membeli produk. 
								Cara ini dipandang efektif untuk membangun koneksi baru dengan calon pelanggan, serta mendorong mereka menjadi pelanggan setia bisnis Anda. Logikanya, 
								jika keterlibatan pelanggan pada bisnis Anda meningkat, peluang tingkat konversi pelanggan untuk membeli produk Anda juga semakin besar.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- End mBanner-->
				
				
				<!-- hText -->
				<div class="section padding_lay">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h2 class="hText">MANFAAT LAINNYA</h2>
							</div>
						</div>
					</div>
				</div>
				<!-- End hText -->
				
				<!-- mSecond m(manfaat)-->
					<div class="section mSecond">
						<div class="container">
							<div class="row">
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10">
											<img class="img100px" src="assets/img/mSecond/person.png">
										</div>
										<div class="full">
											<h5><b>Bersifat Pribadi</b></h5>
										</div>
										<div class="full">
											Email marketing yang bersifat personal ini akan memberikan 
											perhatian yang lebih besar dari pelanggan atau calon pelanggan. 
											Saat pelanggan atau calon pelanggan merasa nyaman menerima email dari anda, 
											maka akan lebih mudah untuk menawarkan apapun pada mereka.
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10">
											<img class="img100px" src="assets/img/mSecond/mail.png">
										</div>
										<div class="full">
											<h5><b>Informasi Cepat</b></h5>
										</div>
										<div class="full">
											Adanya email marketing dapat mempermudah segala hal termasuk menjangkau 
											para pelanggan secara mudah dan cepat pula karena menggunakan email yang 
											langsung dapat diterima dimanapun anda berada.
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10">
											<img class="img100px" src="assets/img/mSecond/money.png">
										</div>
										<div class="full">
											<h5><b>Hemat Biaya</b></h5>
										</div>
										<div class="full">
											Email marketing membuat biaya yang ditanggung lebih ringan dibanding dilakukan 
											secara manual seperti lewat pos atau surat fisik lainnya. Selain itu,  
											tingkat responsif dari pelanggan lebih tinggi jika menggunakan email marketing.
										</div>
									</div>
								</div>
								
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10">
											<img class="img100px" src="assets/img/mSecond/partner.png">
										</div>
										<div class="full">
											<h5><b>Mudah Di Bagikan</b></h5>
										</div>
										<div class="full">
											Adanya email marketing, membuat para pelanggan dapat berbagi segala penawaran 
											produk serta membagikannya kepada teman-teman yang lain. Pelanggan yang berbagi email, 
											bertindak sebagai pendukung dari merek produk anda.
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10">
											<img class="img100px" src="assets/img/mSecond/crm.png">
										</div>
										<div class="full">
											<h5><b>Membangun Relasi</b></h5>
										</div>
										<div class="full">
											Kita bisa dengan mudah menjalin hubungan baik dengan para pelanggan dan calon pelanggan 
											melalui email marketing. Email yang kita kirimkan bisa berupa tips, informasi penting, 
											promosi produk, dan lain-lain, dengan cara yang lebih mudah.
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-4">
									<div class="mSecondLay">
										<div class="full margin-bottom_10	">
											<img class="img100px" src="assets/img/mSecond/viral.png">
										</div>
										<div class="full">
											<h5><b>Promosi Tertarget</b></h5>
										</div>
										<div class="full">
											Dengan email marketing, memungkinkan untuk melakukan promosi tertarget. Maksudnya, 
											kamu bisa memasarkan jasa atau produk kepada konsumen yang benar-benar memerlukan 
											jasa atau produk yang kita pasarkan.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<!-- End mSecond -->
				
				<!-- sContact -->
				<div class="section sContact  wFont">
					<div class="container">
						<div class="row">
							<div class="col-lg-8">
								<div class="row">
									<div class="col-lg-12 margin-bottom_10">
									KONTAK
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/location.png">
										</div>
										<div class="full">
											<a href="https://www.google.com/maps/place/8%C2%B040'53.8%22S+115%C2%B010'47.3%22E/@-8.6817107,115.1788095,19z/data=!4m6!3m5!1s0!7e2!8m2!3d-8.6816039!4d115.1798098"  target="_blank">Jalan Tangkuban Perahu no.40
											<br> Denpasar-Bali</a>
											<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/mail.png">
										</div>
										<div class="full">
										<a href="mailto: pandedonyy@gmail.com">pandedonyy@gmail.com</a>
										<p>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="full">
											<img class="img25px" src="assets/img/sContact/phone.png">
										</div>
										<div class="full">
										<a href="tel: +6287761530973">+62 87 7615 30973</a>
										<p>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<form method="pos" id="berlangganan" action="db/simpansubs.php">
									<div class="full margin-bottom_10">
										BERLANGGANAN
									</div>
									<div class="full">
										<input class="form-control mr-sm-0 emailsubs" type="email" name="emailsubs" placeholder="Your Email" aria-label="urmail" required>
									</div>
									<div class="full">
										<input type="submit" class="btn btn-success my-2 my-sm-0" id="btn-sub" >
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- End sContact -->
				
				<!-- Footer -->
				<div class="section footer wFont">
					&copy; 2020 Cloud Blast. All Rights Reserved.
				</div>
				<!-- End Footer -->
				
				
			</div>
			
				
			
		</main>
		
		
	

	<script type="text/javascript" src="style/js/jquery-3.5.1.js"></script>
	<script type="text/javascript" src="style/js/popper.js"></script>
	<script type="text/javascript" src="style/js/bootstrap.js"></script>
	<script type="text/javascript" src="style/js/cbmain.js"></script>
	</body>
</html>